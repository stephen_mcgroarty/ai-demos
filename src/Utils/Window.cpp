#include "Utils/Window.h"



Window::~Window()
{
	SDL_Quit();
}	


Window::Window(const char * title,unsigned int width,unsigned int height)
{
	SDL_Init(SDL_INIT_VIDEO);


	m_Width = width;
	m_Height= height;
	m_pWindow = std::unique_ptr<SDL_Window,DeleteSDLWindow>(SDL_CreateWindow(title,
								SDL_WINDOWPOS_CENTERED,
								SDL_WINDOWPOS_CENTERED,
								width,
								height,
								SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN));


	m_pRenderer = std::unique_ptr<SDL_Renderer,DeleteSDLRenderer>(SDL_CreateRenderer(m_pWindow.get(),-1,SDL_RENDERER_ACCELERATED));
}


void Window::Clear(Uint8 red,Uint8 green, Uint8 blue)
{
	SDL_SetRenderDrawColor( m_pRenderer.get(), red,green,blue, 255 );
    SDL_RenderClear( m_pRenderer.get() );
}


void Window::SetColour(Uint8 red,Uint8 green,Uint8 blue)
{
	SDL_SetRenderDrawColor(m_pRenderer.get(),red,green,blue,255);
}

void Window::DrawPoints(const std::vector<std::pair<int,int>> & points,int size)
{
	for(auto p : points)
	{
		DrawPoint(p.first,p.second,size);
	}
}

void Window::Finish()
{
	SDL_RenderPresent( m_pRenderer.get() );
}


void Window::DrawPoint(int x,int y,int size)
{
	//Points doesn't give needed scale, use rect instead 
	SDL_Rect r;
	r.x = x;
	r.y = m_Height -y;
	r.w = r.h = size;
	SDL_RenderFillRect( m_pRenderer.get(), &r );
}
