#ifndef WINDOW_H
#define WINDOW_H
#include <SDL.h>
#include <memory>
#include <vector>
class Window
{
public:

	Window(const char * title, unsigned int width,unsigned int height);
	~Window();

	void Clear(Uint8 red,Uint8 green, Uint8 blue);
	void DrawPoints(const std::vector<std::pair<int,int>> & points,int size);

	void SetColour(Uint8 red,Uint8 green,Uint8 blue);
	void Finish();

private: 
	struct DeleteSDLWindow
	{
		inline void operator()(SDL_Window * window){ SDL_DestroyWindow(window); }
	};

	struct DeleteSDLRenderer
	{
		inline void operator()(SDL_Renderer *renderer) { SDL_DestroyRenderer(renderer); }
	};


	int m_Width,m_Height;
	void DrawPoint(int x,int y,int size);
	std::vector<std::pair<int,int>> m_Points;
	std::unique_ptr<SDL_Window,DeleteSDLWindow> m_pWindow;
	std::unique_ptr<SDL_Renderer,DeleteSDLRenderer> m_pRenderer;
};


#endif