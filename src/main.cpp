#include <stdio.h>
#include "Classifier/NaiveBayes.h"
#include <fstream>
#include <sstream>
#include "Utils/Window.h"
#include "Regressions/LinearRegression.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

static void ReadFile(const char * filename,std::vector<std::string> & words)
{
	std::ifstream file(filename,std::ios::binary | std::ios::beg | std::ios::in);
	std::stringstream stream;

	stream << file.rdbuf();

	file.close();
	while(stream.good()) 
	{
		std::string temp;
		stream >> temp;
		words.push_back(temp);
	}
}

static void BreakText(const std::string & string, std::vector<std::string> & data)
{
	std::stringstream stringstream(string);
	std::istream_iterator<std::string> begin{stringstream},end;


	data = {std::vector<std::string>(begin, end) };

}

static void RunClassifier() 
{
	std::unique_ptr<Classifier> classifier{new NaiveBayes()};

 	std::vector< std::pair<std::string,std::vector<std::string> > > trainingData;	
	std::vector<std::string> englishWords,frenchWords,germanWords;
	

 	ReadFile("../data/training.language.en.txt",englishWords);
 	ReadFile("../data/training.language.fr.txt",frenchWords);
 	ReadFile("../data/training.language.de.txt",germanWords);
 	
 	trainingData.push_back(std::pair<std::string,std::vector<std::string>>("English",englishWords));
 	trainingData.push_back(std::pair<std::string,std::vector<std::string>>("French",frenchWords));
 	trainingData.push_back(std::pair<std::string,std::vector<std::string>>("German",germanWords));

	classifier->Fit(trainingData);

	std::vector<std::vector<std::string> > testDataSet;
	testDataSet.push_back(std::vector<std::string>());
	BreakText("this is english",testDataSet.back());

	testDataSet.push_back(std::vector<std::string>());
	BreakText("ce est francais",testDataSet.back());


	testDataSet.push_back(std::vector<std::string>());
	BreakText("dies ist deutsch",testDataSet.back());
	
	
	testDataSet.push_back(std::vector<std::string>());
	BreakText("what language is this",testDataSet.back());

	testDataSet.push_back(std::vector<std::string>());
	BreakText("quelle langue est ce",testDataSet.back());


	testDataSet.push_back(std::vector<std::string>());
	BreakText("in welcher haben sie diese",testDataSet.back());

	
	/*testDataSet.push_back(std::vector<std::string>());
	testDataSet.back().push_back("today");
	testDataSet.back().push_back("is");
	testDataSet.back().push_back("secret");*/


	for( auto & feature : testDataSet)
	{
		std::string prediction;
		classifier->Classify(feature,prediction);

		printf("Classified: \"");
		for(auto & str : feature)
		{
			printf("%s ",str.c_str() );
		}
		printf("\" as %s\n",prediction.c_str() );
	}
}

	static void SetupRegressionData(std::vector<std::pair<int,int>> & data)
	{
	  	srand (time(NULL));

		for(int x = 50; x < 750; x += 3)
		{
			data.push_back(std::pair<int,int>(x+(rand() % 100), 400 + (rand() % 100) ));
		}
	}


int main(int argc,char *argv[])
{
	if( argc > 4)	RunClassifier();

	Window window("Test",800,800);
	std::vector<std::pair<int,int>>  data;
	LinearRegression regression(&window);

	SetupRegressionData(data);

	printf("Enter any char run the regression\n");
	getchar();
	regression.Regress(data);

	//window.Draw();
	printf("Enter any char to quit\n");
	getchar();

	getchar();
	return 0;
}
