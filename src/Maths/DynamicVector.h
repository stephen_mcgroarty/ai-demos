#ifndef DYNAMIC_VECTOR_H
#define DYNAMIC_VECTOR_H


/*
	Vector with the ability to dynamically resize at runtime while keeping the mathematical 
	methods extended by the static vector.
*/
template<typename T>
class DynamicVector
{
public:
	DynamicVector(unsigned int size);
	DynamicVector(const DynamicVector & copy);

	inline T Magnitude() const;

	void Normalize();
	DynamicVector<T> GetNormalized() const;

	void Resize(unsigned int size);

	T & operator[](unsigned int index) { return m_pData.get()[index]; }
	const T & operator[](unsigned int index) const { return m_pData.get()[index];}

	DynamicVector<T> &operator=(const DynamicVector<T> & rhs);

	T Dot(const DynamicVector<T> & other) const;


	DynamicVector<T> operator*(T scalar) const;
	void operator *=    (T scalar);


	DynamicVector<T> operator+ (const DynamicVector<T> & other) const;
	DynamicVector<T> operator- (const DynamicVector<T> & other) const;
	DynamicVector<T> operator* (const DynamicVector<T> & other) const;
	DynamicVector<T> operator/ (const DynamicVector<T> & other) const;



	void Print()
	{
		for(unsigned int iElem = 0; iElem < m_Size; ++iElem)
		{
			printf(" %f ",m_pData.get()[iElem]);
		}
		printf("\n");
	}

	void operator+=(const DynamicVector<T> & other);
	void operator-=(const DynamicVector<T> & other);
	void operator*=(const DynamicVector<T> & other);
	void operator/=(const DynamicVector<T> & other);
private:
	std::unique_ptr<T> m_pData;
	unsigned int m_Size;

};


template<typename T>
DynamicVector<T>::DynamicVector(unsigned int size)
{
	m_pData= std::unique_ptr<T>(new T[size]);
	m_Size = size;
}

template<typename T>
DynamicVector<T>::DynamicVector(const DynamicVector & copy)
{
	m_Size = copy.m_Size;
	m_pData= std::unique_ptr<T>(new T[m_Size]);

	for(unsigned int iElem = 0; iElem < m_Size; ++iElem)
	{
		m_pData.get()[iElem] = copy.m_pData.get()[iElem];
	}
}


template<typename T>
inline T DynamicVector<T>::Magnitude() const 
{
	T sum;
	for(unsigned int iElem = 0; iElem < m_Size; ++iElem)
	{
		if( iElem == 0 )
			sum = m_pData.get()[iElem]*m_pData.get()[iElem];
		else 
			sum += m_pData.get()[iElem]*m_pData.get()[iElem];
	}


	return sqrt(sum);
}


template<typename T>
void DynamicVector<T>::Normalize()
{
	T size = this->Magnitude();

	for(unsigned int iElem = 0; iElem < m_Size; ++iElem)
	{
		m_pData.get()[iElem] /= size;
	}

}


template<typename T>
DynamicVector<T> DynamicVector<T>::GetNormalized() const
{
	DynamicVector newVector = {*this};
	newVector.Normalize();
	return newVector;
}




template<typename T>
T DynamicVector<T>::Dot(const DynamicVector<T>  & other) const
{
	T sum;

	for(unsigned int iElem = 0; iElem < m_Size; ++iElem)
	{
		if( iElem == 0 )
		{
			sum = m_pData.get()[iElem]*other.m_pData.get()[iElem];
		}
		else
		{
			sum += m_pData.get()[iElem]*other.m_pData.get()[iElem];
		}
	}
	return sum;
}

template<typename T>
DynamicVector<T> DynamicVector<T>::operator*(T scalar) const
{
	DynamicVector vector(m_Size);

	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		vector[iElem] = m_pData.get()[iElem]*scalar;
	}

	return vector;
}
	

template<typename T>
void DynamicVector<T>::operator *=(T scalar) 
{
	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		m_pData.get()[iElem] *= scalar;
	}
}

template<typename T>
DynamicVector<T> DynamicVector<T>::operator+(const DynamicVector<T> & other) const
{
	DynamicVector vector(m_Size);

	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		vector[iElem] = m_pData.get()[iElem]+other.m_pData.get()[iElem];
	}

	return vector;
}

template<typename T>
DynamicVector<T> DynamicVector<T>::operator- (const DynamicVector<T> & other) const
{
	DynamicVector vector(m_Size);

	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		vector[iElem] = m_pData.get()[iElem]-other.m_pData.get()[iElem];
	}

	return vector;
}



template<typename T>
DynamicVector<T> DynamicVector<T>::operator* (const DynamicVector<T> & other) const
{	
	DynamicVector vector(m_Size);

	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		vector[iElem] = m_pData.get()[iElem]*other.m_pData.get()[iElem];
	}

	return vector;
}

template<typename T>
DynamicVector<T> DynamicVector<T>::operator/ (const DynamicVector<T> & other) const
{
	DynamicVector vector(m_Size);

	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		vector[iElem] = m_pData.get()[iElem]/other.m_pData.get()[iElem];
	}

	return vector;
}

template<typename T>
void DynamicVector<T>::operator+=(const DynamicVector<T> & other)
{
	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		m_pData.get()[iElem] += other.m_pData.get()[iElem];
	}
}


template<typename T>
void DynamicVector<T>::operator-=(const DynamicVector<T> & other)
{
	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		m_pData.get()[iElem] -= other.m_pData.get()[iElem];
	}
}



template<typename T>
DynamicVector<T> & DynamicVector<T>::operator=(const DynamicVector<T> & rhs)
{
	m_Size = rhs.m_Size;
	m_pData= std::unique_ptr<T>(new T[m_Size]);
	
	for(unsigned int iElem = 0; iElem < m_Size; ++iElem)
	{
		m_pData.get()[iElem] = rhs.m_pData.get()[iElem];
	}
	return *this;
}


template<typename T>
void DynamicVector<T>::operator*=(const DynamicVector<T>& other)
{
	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		m_pData.get()[iElem] *= other.m_pData.get()[iElem];
	}
}

template<typename T>
void DynamicVector<T>::operator/=(const DynamicVector<T> & other)
{
	for(unsigned int iElem =0; iElem<m_Size; ++iElem)
	{
		m_pData.get()[iElem] /= other.m_pData.get()[iElem];
	}
}



#endif 