#ifndef LINEAR_REGRESSION_H
#define LINEAR_REGRESSION_H
#include "Utils/Window.h"

class LinearRegression
{
public:
	LinearRegression(Window * windowCallback);

	float CostFunction(const std::vector<std::pair<float,float>> & data,float theta1,float theta2) const; 
	float Hypothesis(float x,float theta1,float theta2) const;
	void Regress(const std::vector<std::pair<int,int>> & data);

	void BatchGradientDescent(const std::vector<std::pair<float,float>> & data,float & theta1,float & theta2);
private:
	Window * m_pWindow;
	float m_Theta1,m_Theta2;
	float m_LearningRate;
};

#endif