#include "Regressions/LinearRegression.h"

LinearRegression::LinearRegression(Window * windowCallback): 
	m_pWindow(windowCallback), 
	m_Theta1(0.0f),
	m_Theta2(0.0f),
	m_LearningRate(0.01f)
{}


float LinearRegression::Hypothesis(float x,
								  float theta1,
								  float theta2) const
{
	return theta1 + x*theta2;
}


void LinearRegression::BatchGradientDescent(const std::vector<std::pair<float,float>> & data,
									 float& theta1,
									 float& theta2)
{

	float weight = (1.0f/static_cast<float>(data.size()));
	float theta1Res = 0.0f;
	float theta2Res = 0.0f;

	for(auto p: data)
	{

		float cost = Hypothesis(p.first,theta1,theta2) - p.second;
		theta1Res += cost;
		theta2Res += cost*p.first;
	}	

	theta1 = theta1 - (m_LearningRate*weight* theta1Res);
	theta2 = theta2 - (m_LearningRate*weight* theta2Res);
}




float LinearRegression::CostFunction(const std::vector<std::pair<float,float>> & data,
									 float theta1,
									 float theta2) const
{ 
	float error = 0.0f;
	for(auto p: data)
	{

		float prediction = (Hypothesis(p.first,theta1,theta2) - p.second) ;
		error += prediction*prediction;
	}

	error *= 1.0f/(data.size()*2.0f);
	return error;
}

void LinearRegression::Regress(const std::vector<std::pair<int,int>> & inData)
{
	std::vector<std::pair<float,float>> data(inData.size());

	for(unsigned int index = 0; index < inData.size(); ++index)
	{
		data[index].first  = inData[index].first /800.0f;
		data[index].second = inData[index].second/800.0f;
	}
	int otherItr =0;
	for(unsigned int itr = 0; itr < 2500; ++itr)
	{
		otherItr++;
		std::vector<std::pair<int,int>> line;

		float costBefore = CostFunction(data,m_Theta1,m_Theta2);
		BatchGradientDescent(data,m_Theta1,m_Theta2);
		printf("Cost before %f, cost after %f, difference %f\n",costBefore,CostFunction(data,m_Theta1,m_Theta2),CostFunction(data,m_Theta1,m_Theta2) -costBefore);

		if( otherItr > 1)
		{
			m_pWindow->Clear(255,255,255);

			for(float x = 0.0f; x < 1.0f; x+=0.001f)
			{
				int y = static_cast<int>(Hypothesis(static_cast<float>(x),m_Theta1,m_Theta2)*800.0f);

				line.push_back(std::pair<int,int>(static_cast<int>(x*800.0f),y));
			}

			m_pWindow->SetColour(0,255,0);
			m_pWindow->DrawPoints(line,3);


			m_pWindow->SetColour(255,0,0);
			m_pWindow->DrawPoints(inData,3);
			m_pWindow->Finish();
			otherItr = 0;
		}
	}
	/*	m_pWindow->Clear(255,255,255);

	for(float x = 0.0f; x < 800.0f; ++x)
	{
		float y = Hypothesis(x,m_Theta1,m_Theta2);
		line.push_back(std::pair<int,int>(static_cast<int>(x),static_cast<int>(y)));
	}

	m_pWindow->SetColour(0,255,0);
	m_pWindow->DrawPoints(line,3);


	m_pWindow->SetColour(255,0,0);
	m_pWindow->DrawPoints(data,3);
	m_pWindow->Finish();*/
}

