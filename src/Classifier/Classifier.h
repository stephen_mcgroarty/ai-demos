#ifndef CLASSIFIER_H
#define CLASSIFIER_H
#include <vector>
#include <string>


class Classifier
{
public:
	virtual ~Classifier() {}

	virtual void Fit(const std::vector< std::pair<std::string,std::vector<std::string> > > & trainingData) = 0;
	//The label is the 
	virtual void Classify(const std::vector<std::string> & testData,std::string & returnedLabel) = 0;
};

#endif