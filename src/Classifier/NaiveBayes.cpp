#include "NaiveBayes.h"



NaiveBayes::NaiveBayes(): m_LaplaceSmoother(1.0f),m_TotalNumberOfFeatures(0)
{
}

void NaiveBayes::Fit(const std::vector< std::pair<std::string,std::vector<std::string> > > & trainingData)
{

	std::map<std::string,std::map<std::string,unsigned int>> uniqueElems;
	//Loop over each label in training data (not necessarily unique)
	for(auto & pair : trainingData)
	{
		if( m_FeaturesPerCategory.find(pair.first) == m_FeaturesPerCategory.end() )
		{
			m_FeaturesPerCategory.insert(std::pair<std::string,unsigned int>(pair.first,0));
		}


		for(auto & str : pair.second)
		{
			m_FeaturesPerCategory[pair.first] += 1;
			m_TotalNumberOfFeatures           += 1;
		
			uniqueElems[pair.first][str] += 1;
		}
	}


	for(auto & pair : uniqueElems)
	{
		m_TotalNumberOfUniqueFeatures += pair.second.size();
	}

	//For each unique label in the set
	for(auto & pair : m_FeaturesPerCategory)
	{
		//m_ProbLabel[pair.first] = static_cast<float>(pair.second)/(totalNumber);
		//printf("\t%d+%f/(%d + %f*%d)\n",pair.second,m_LaplaceSmoother,m_TotalNumberOfFeatures,m_LaplaceSmoother,m_FeaturesPerCategory.size());

		m_ProbLabel[pair.first] = (static_cast<float>(pair.second)+m_LaplaceSmoother)/(m_TotalNumberOfFeatures+m_LaplaceSmoother*m_FeaturesPerCategory.size());
	}


	//For each set of words with label
	for(auto & pair : trainingData)
	{	
		//Number of words associated with each label
		std::map<std::string,float> & bagOfWords = m_Probabilities[pair.first]; 
		
		std::vector<std::string> wordsInLabel = pair.second;


		for(auto & word : wordsInLabel)
		{
			//if( bagOfWords.find(word) != bagOfWords.end())
			//	continue;

			unsigned int occurences = uniqueElems[pair.first][word];
			//bagOfWords[word] = static_cast<float>(occurences)/(featuresPerCategory[pair.first]);

			bagOfWords[word] = (static_cast<float>(occurences) + m_LaplaceSmoother)/(m_FeaturesPerCategory[pair.first] + m_LaplaceSmoother*m_TotalNumberOfUniqueFeatures);
		}
	}
}



void NaiveBayes::Classify(const std::vector<std::string> & data,std::string & label)
{
	//No prior prob should be less than zero so we should be ok
	float maxProb = 0.0f;
	for(auto & pair : m_Probabilities)
	{		 
		//  P(Label:data) = (P(data:label).P(label))/(P(data:label).P(label) + p(data:nLabel).P(nLabel))
		std::map<std::string,float> & bagOfWords = m_Probabilities[pair.first]; 

		float prob = m_ProbLabel[pair.first];

		//printf("P(%s): %f\n",pair.first.c_str(),m_ProbLabel[pair.first] );
		for(auto & word : data)
		{	

			if( bagOfWords.find(word) == bagOfWords.end())
			{
				//printf("\t%f/(%d + %f*%d)\n",m_LaplaceSmoother,m_FeaturesPerCategory[pair.first],m_LaplaceSmoother,m_TotalNumberOfUniqueFeatures);

				bagOfWords[word] = (m_LaplaceSmoother)/(m_FeaturesPerCategory[pair.first] + m_LaplaceSmoother*m_TotalNumberOfUniqueFeatures);
			}

			//printf("\tP(%s| %s): %f\n",word.c_str(),pair.first.c_str(),bagOfWords[word] );

			prob *= bagOfWords[word];
		}

		float notProb = 0.0f;


		for(auto & notPair : m_Probabilities)
		{
			if( pair == notPair ) continue;

			std::map<std::string,float> & notBagOfWords = m_Probabilities[notPair.first]; 


			//Prior prob of label occuring
			float notProbLabel = m_ProbLabel[notPair.first];
			//printf("\n\tP(%s): %f\n",notPair.first.c_str(),notProbLabel );

			for(auto & word : data)
			{
				//Prob of word occuring in label


				if( notBagOfWords.find(word) == notBagOfWords.end())
				{
					//printf("\t%f/(%d + %f*%d)\n",m_LaplaceSmoother,m_FeaturesPerCategory[notPair.first],m_LaplaceSmoother,m_TotalNumberOfUniqueFeatures);
					notBagOfWords[word] = (m_LaplaceSmoother)/(m_FeaturesPerCategory[notPair.first] + m_LaplaceSmoother*m_TotalNumberOfUniqueFeatures);
				}
				//printf("\tP(%s| %s): %f\n",word.c_str(),notPair.first.c_str(),notBagOfWords[word] );
				notProbLabel *= notBagOfWords[word];
			}


			notProb += notProbLabel;
		}



		//Bayes rule 
		float posterior = prob/(prob + notProb);
		if( posterior > maxProb )
		{
			maxProb = posterior;
			label = pair.first;
		}

		//printf("P(%s|message): %f\n",pair.first.c_str(),posterior );
	}
}
