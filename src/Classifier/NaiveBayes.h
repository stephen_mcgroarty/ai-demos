#ifndef NAIVE_BAYES_H
#define NAIVE_BAYES_H
#include "Classifier/Classifier.h"
#include <map>
#include <string>



class NaiveBayes: public Classifier
{
public:	

	NaiveBayes();
	void Fit(const std::vector< std::pair<std::string,std::vector<std::string> > > & trainingData);
	void Classify(const std::vector<std::string> & features,std::string & label);

private:

	//m_Probabilities[Label] = probability[feature]
	std::map<std::string,std::map<std::string,float>> m_Probabilities;


	float m_LaplaceSmoother;


	//Number of features in each category
	std::map<std::string,unsigned int> m_FeaturesPerCategory;


	unsigned int m_TotalNumberOfFeatures;
	unsigned int m_TotalNumberOfUniqueFeatures; //Length of the global dictionary
	//prior probaility of each given label
	std::map<std::string,float> m_ProbLabel;
};
#endif
